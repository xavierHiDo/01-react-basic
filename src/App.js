import logo from './logo.svg';
import './App.css';
import Basic from './components/Basic';
import Property from './components/Property';
import State from './components/State';
import ConditionalRendering from './components/ConditionalRendering';
import RenderingElements from "./components/RenderingElements";
import { EventsES6, EventsES7, MoreAboutEvents } from "./components/Events";
import Communication from './components/Communication';
import Lifecycle from './components/Lifecycle';
import AjaxApi from './components/AjaxApi';
import CounterHook from './components/CounterHook';
import ScrollHook from './components/ScrollHook';
import ClockHook from './components/ClockHook';
import AjaxHook from './components/AjaxHook';
import CustomHook from './components/CustomHook';
import Reference from './components/Reference';
import Form from './components/Form';
import Style from './components/Style';
import StylizedComponents from './components/StylizedComponents';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <section>
          <img src={logo} className="App-logo" alt="logo" />
          <p>
          Editar <code>src/App.js</code> and save to reload.
          </p>
          <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
          >
          Learn React
          </a>
        </section>
        <section>
          <Basic msg="Hola soy un componente"></Basic><hr />
          <Property
            string="Esto es una cadena de texto"
            number={19}
            booleano={true}
            array={[1, 2, 3]}
            object={{ nombre: "Javier", correo: "javi@gmail.com" }}
            funcion={(num) => num * num}
            reactElement={<i>Esto es un elemento React</i>}
            reactComponent={
              <Basic msg="Componente pasado como prop"/>
            }
          /><hr />
          <State data={[1,2,3]}/><hr />
          <ConditionalRendering/><hr />
          <RenderingElements/><hr />
          <EventsES6/><hr />
          <EventsES7/><hr />
          <MoreAboutEvents/>
          <Communication/><hr />
          <Lifecycle/><hr />
          <AjaxApi/><hr />
          <CounterHook/><hr />
          <ScrollHook/><hr />
          <ClockHook/><hr />
          <AjaxHook/><hr />
          <CustomHook/><hr />
          <Reference/><hr />
          <Form/><hr />
          <Style/><hr />
          <StylizedComponents/><hr />
        </section>
      </header>
    </div>
  );
}

export default App;