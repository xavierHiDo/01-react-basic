import React, {useState, useEffect} from 'react'

export default function ScrollHook(props) {
  const [scrollY, setScrollY] = useState(0);

  useEffect(() => { //componentDidUpdate
    console.log('Moviendo scrollY');

    //Creamos una función para actualizar el estado
    const actualizarScrollY = () => {
      let scrollY = window.pageYOffset;
      console.log(`scrollY: ${scrollY}`);
      setScrollY(scrollY);
    };

    //Actualizamos el scroll al montar el componente
    actualizarScrollY();

    //Nos suscribimos al evento scroll de window
    window.addEventListener("scroll", actualizarScrollY);

    return () => {
      window.removeEventListener("scroll", actualizarScrollY);
    };
  }, [scrollY]);

  useEffect(() => {
    console.log('Fase de Montaje');
  }, []);

  useEffect(() => {  // componentDidUpdate
    console.log('Fase de Actualización');
  });

  useEffect(() => {
    return () => {
      console.log('Fase de Desmontaje');
    };
  });

  return(
    <>
      <h2>Hooks - useEffect y ciclo de vida</h2>
      <p>Scroll (Y) del navegador {scrollY} px</p>
    </>
  )
}