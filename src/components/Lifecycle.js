import React, { Component} from 'react';

export default class Lifecycle extends Component {
  constructor(props) {
    super(props);
    console.log(0, 'Montaje - El componente se inicializa, aun no esta en el DOM');

    this.state = {
      hour: new Date().toLocaleTimeString(),
    };
    this.timer = null;
  }

  componentDidMount() {
    console.log(1, 'Montaje - El componente ya se encuentra en el DOM');
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(2, 'Actualización - El estado o las props del componente ham cambiado');
    console.log('prevProps', prevProps);
    console.log('prevState', prevState);
  }

  componentWillUnmount() {
    console.log(3, 'Desmontaje - El componente ha sido eliminado del DOM');
  }

  tictac = () => {
    this.timer = setInterval(() => {
      this.setState({
        hour: new Date().toLocaleTimeString(),
      });
    }, 1000);
  }

  start = () => {
    this.tictac();
  }

  stop = () => {
    clearInterval(this.timer);
  }

  render() {
    console.log(4, 'Actualización - El componente se redibuja, cambios en DOM');
    return (
      <>
        <h2>Ciclo de vida de un componente de clase</h2>
        <h3>{this.state.hour}</h3>
        <button onClick={this.start}>Iniciar</button>
        <button onClick={this.stop}>Detener</button>
      </>
    )
  }
}