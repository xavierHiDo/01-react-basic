import React, { Component} from 'react';

// class Basic extends Component {
//   render() {
//     return <h2>{this.props.msg}</h2>
//   }
// }

// function Basic(props) {
//   return <h2>{props.msg}</h2>
// }

const Basic = (props) => <h2>{props.msg}</h2>

export default Basic;