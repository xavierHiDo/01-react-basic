import React, { Component} from 'react';

export default class Father extends Component {
  state = {
    contador: 0,
  };

  incrementCounter = (e) => {
    this.setState({
      contador: this.state.contador + 1,
    })
  }

  render() {
    return (
      <>
        <h2>Comunicacion entre componentes</h2>
        <p>{this.state.contador}</p>
        <button onClick={this.incrementCounter}>Sumar desde padre</button>
        <Son message="Mensaje hijo" incrementCounter={this.incrementCounter}/>
      </>
    )
  }
}

function Son(props) {
  return(
    <>
      <h3>{props.message}</h3>
      <button onClick={props.incrementCounter}>Sumar desde hijo</button>
    </>
  )
}