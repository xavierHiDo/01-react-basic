import React, { useState, useEffect } from "react";

function Reloj({ hora }) {
  return <h3>{hora}</h3>;
}

export default function ClockHook() {
  const [hora, setHora] = useState(new Date().toLocaleTimeString());
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    let temporizador;

    if (visible) {
      temporizador = setInterval(() => {
        setHora(new Date().toLocaleTimeString());
      }, 1000);
    } else {
      clearInterval(temporizador);
    }

    return () => {
      console.log("Fase de Desmontajeeeeee");
      clearInterval(temporizador);
    };
  }, [visible]);

  return (
    <>
      <h2>Reloj con Hooks</h2>
      {visible && <Reloj hora={hora} />}
      <button onClick={() => setVisible(true)}>iniciar</button>
      <button onClick={() => setVisible(false)}>detener</button>
    </>
  );
}

// const Reloj2 = ({ visible }) => {
//   const [hour, setHour] = useState(new Date().toLocaleTimeString());

//   useEffect(() => {
//       let timer;
//       if (visible) {
//           console.log("Fase de Inicializacion");
//           timer = setInterval(() => {
//               setHour(new Date().toLocaleTimeString())
//           }, 1000);
//       }
//       else {
//           clearInterval(timer);
//       }
//       return () => {
//           clearInterval(timer);
//           console.log("Fase de Desmontaje");
//       }
//   }, [visible])
//   return (
//       <>
//           <h3>{hour}</h3>
//       </>
//   )

// }

// export const Reloj2Hooks = () => {
//   const [visible, setVisible] = useState(false);

//   useEffect(() => {
//       console.log("Fase de Montaje");
//   }, [])
//   return (
//       <div>
//           <h2>Reloj2 con Hooks</h2>
//           {(visible) && <Reloj2 visible={visible} />}
//           <button onClick={() => setVisible(true)}>Iniciar</button>
//           <button onClick={() => setVisible(false)}>Detener</button>
//       </div>
//   )
// }