import React from 'react';
import PropTypes from 'prop-types';

export default function Property(props) {
  return (
    <div>
      <h2>{props.porDefecto}</h2>
      <ul>
        <li>{props.string}</li>
        <li>{props.number}</li>
        <li>{props.booleano ? "Verdadero" : "Falso"}</li>
        <li>{props.array.join(", ")}</li>
        <li>{props.object.nombre + " - " + props.object.correo}</li>
        <li>{props.array.map(props.funcion).join(", ")}</li>
        <li>{props.reactElement}</li>
        <li>{props.reactComponent}</li>
      </ul>
    </div>
  )
}

Property.defaultProps = {
  porDefecto: "Las props",
}

Property.propTypes = {
  data: PropTypes.array.isRequired
}