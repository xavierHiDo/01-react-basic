import React, {useState} from 'react'

export default function CounterHook(props) {
  const [counter, setCounter] = useState(0)
  const deduct = () => setCounter(counter -1)
  return(
    <>
      <h2>{props.title}</h2>
      <h3>{counter}</h3>
      <button onClick={() => setCounter(counter + 1)}>+</button>
      <button onClick={deduct}>-</button>
    </>
  )
}

CounterHook.defaultProps = {
  title: "Hooks - useState",
}